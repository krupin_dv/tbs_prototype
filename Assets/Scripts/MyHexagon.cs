using UnityEngine;
using System.Collections.Generic;

public class MyHexagon : Hexagon
{
    public GroundType GroundType;
    private Cover cover = Cover.Fog;

    public void Start()
    {
        // SetHighlighterColor(new Color(1,1,1,0));
    }

    public override void MarkAsReachable()
    {
        SetHighlighterColor(new Color(1, 0.92f, 0.016f, 0.25f));
    }
    public override void MarkAsPath()
    {
        SetHighlighterColor(new Color(0,1,0,1));
    }
    public override void MarkAsHighlighted()
    {
        SetHighlighterColor(new Color(0.5f,0.5f,0.5f,0.25f));
    }
    public override void UnMark()
    {
        SetHighlighterColor(new Color(1,1,1,0));
    }

    public override Cover Cover
    {
        get
        {
            return cover;
        }
        set
        {
            cover = value;
            var cover_layer = transform.FindChild("Cover");
            var spriteRenderer = cover_layer.GetComponent<SpriteRenderer>();
            
            if (value == Cover.None)
                spriteRenderer.color = new Color(1,1,1,0); // visible
            else
                spriteRenderer.color = new Color(0.5f,0.5f,0.5f,0.5f); // Shadow
        }
    }
    private void SetHighlighterColor(Color color)
    {
        var highlighter = transform.FindChild("Highlighter");
        var spriteRenderer = highlighter.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.color = color;
        }
        foreach (Transform child in highlighter.transform)
        {
            var childColor = new Color(color.r,color.g,color.b,1);
            spriteRenderer = child.GetComponent<SpriteRenderer>();
            if (spriteRenderer == null) continue;

            child.GetComponent<SpriteRenderer>().color = childColor;
        }
    }

    public override Vector3 GetCellDimensions()
    {
        var ret = GetComponent<SpriteRenderer>().bounds.size;
        return ret*0.98f;
    }
}

public enum GroundType
{
    Land,
    Water
};
public enum Cover
{
    None, // Visible
    Shadow,
    Fog, // Fog of war
};
