﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MainCamera : MonoBehaviour {

    // Создание переменных для кнопок
    public List<KeyCode> upButton;
    public List<KeyCode> downButton;
    public List<KeyCode> leftButton;
    public List<KeyCode> rightButton;
    //public List<KeyCode> scrollUpButton;
    //public List<KeyCode> scrollDownButton;

    public const float camSpeed = 0.5f;

    public const int forwardTreshold = -5;
    public const int backTreshold = -40;
    private float leftTreshold;
    private float rightTreshold;
    private float topTreshold;
    private float bottomTreshold;

    private Camera  goCamera;
    private Vector3 newCamPos;

    // Use this for initialization
    void Start () {
        // Инициализируем кординаты мышки и ищем главную камеру
        goCamera = GetComponent<Camera>();
        //go    = goCamera.transform.parent.gameObject;

        var grid = GameObject.FindObjectOfType<CellGrid>();
        var first_cell = grid.transform.GetChild(0);
        var last_cell = grid.transform.GetChild(grid.transform.GetChildCount() - 1);

        leftTreshold = first_cell.transform.position.x;
        bottomTreshold = first_cell.transform.position.y;
        rightTreshold = last_cell.transform.position.x;
        topTreshold = last_cell.transform.position.y;
    }

    // Update is called once per frame
    void Update () {
        // Необходимое движение
        Vector3 movement = new Vector3();
        // Проверка нажатых клавиш
        movement += MoveIfPressed(upButton, Vector3.up);
        movement += MoveIfPressed(downButton, Vector3.down);
        movement += MoveIfPressed(leftButton, Vector3.left);
        movement += MoveIfPressed(rightButton, Vector3.right);
        if(Input.mouseScrollDelta.y > 0)
            movement += Vector3.forward;
        else if (Input.mouseScrollDelta.y < 0)
            movement += Vector3.back;

        // Если нажато несколько кнопок, обрабатываем это
        movement.Normalize(); // (1, 1, 0) -> (0.7, 0,7, 0) - т.е. величина смещения будет равна 1
        // Перемещаем камеру
        if (movement.magnitude > 0) {
            newCamPos = goCamera.transform.position += movement * camSpeed; // текущее положение + новое
            newCamPos = CheckThresholds(newCamPos);
            goCamera.transform.position = newCamPos;
        }
    }

    // Возвращает движение, если нажата кнопка
    Vector3 MoveIfPressed(List<KeyCode> keyList, Vector3 Movement) {
        // Проверяем кнопки из списка
        foreach (KeyCode element in keyList)
            if(Input.GetKey (element)) // Если нажато, покидаем функцию
                return Movement;

        // Если кнопки не нажаты, то не перемещаем
        return Vector3.zero;
    }

    // Если точка находится за пределами, то меняет кординату до максимально возможной
    Vector3 CheckThresholds(Vector3 newCamPos) {
        if (newCamPos.x < leftTreshold)
            newCamPos.x  = leftTreshold;
        else if (newCamPos.x > rightTreshold)
            newCamPos.x  = rightTreshold;

        if (newCamPos.y < bottomTreshold)
            newCamPos.y  = bottomTreshold;
        else if (newCamPos.y > topTreshold)
            newCamPos.y  = topTreshold;

        if (newCamPos.z > forwardTreshold)
            newCamPos.z  = forwardTreshold;
        else if (newCamPos.z < backTreshold)
            newCamPos.z  = backTreshold;

        return newCamPos;
    }
}
